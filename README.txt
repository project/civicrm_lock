# CiviCRM Lock

This module provides a CiviCRM lock implementation backed by the Drupal lock
subsystem (`semaphore` database table).

## Installation

To enable this locking mechanism, add this line to your civicrm.settings.php
file:

```
define('CIVICRM_WORK_LOCK', 'CiviLockDrupal::createScopedLock');
```

## Known issues

Currently this class will break an expired lock and attempt to acquire it, but
does not wait for the lock to be released.  This would be easy enough to add, if
required, by adding a lock_wait() call.
