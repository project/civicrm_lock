<?php

/**
 * @file
 * Enables CiviMail to use the Drupal lock subsystem.
 */

use Civi\Core\Lock\LockInterface;

/**
 * CiviCRM Drupal Lock.
 */
class CiviLockDrupal implements LockInterface {

  public static $jobLog = FALSE;

  /**
   * Let's have a 3 second timeout for now.
   */
  const TIMEOUT = 3;

  /**
   * Drupal locks are persistent so give them a lifetime.
   */
  const LIFETIME = 21600;

  /**
   * True if we've acquired the lock.
   *
   * @var bool
   */
  protected $hasLock = FALSE;

  /**
   * The name of the lock to acquire.
   *
   * @var string
   */
  protected $name;

  /**
   * Use Drupal lock system.
   *
   * @param string $name
   *   Symbolic name for the lock. Names generally look like
   *   "worker.mailing.EmailProcessor" ("{category}.{component}.{AdhocName}").
   *
   *   Categories: worker|data|cache|...
   *   Component: core|mailing|member|contribute|...
   *
   * @return \Civi\Core\Lock\LockInterface
   *   A lock instance.
   */
  public static function createGlobalLock($name) {
    return new static($name, NULL, TRUE);
  }

  /**
   * Use Drupal lock system to create lock unique to this CiviCRM instance.
   *
   * @param string $name
   *   Symbolic name for the lock. Names generally look like
   *   "worker.mailing.EmailProcessor" ("{category}.{component}.{AdhocName}").
   *
   *   Categories: worker|data|cache|...
   *   Component: core|mailing|member|contribute|...
   *
   * @return \Civi\Core\Lock\LockInterface
   *   A lock instance.
   */
  public static function createScopedLock($name) {
    return new static($name);
  }

  /**
   * Use Drupal lock system.
   *
   * Conditionally apply prefix to lock name if civimail_server_wide_lock is
   * disabled.
   *
   * @param string $name
   *   Symbolic name for the lock. Names generally look like
   *   "worker.mailing.EmailProcessor" ("{category}.{component}.{AdhocName}").
   *
   *   Categories: worker|data|cache|...
   *   Component: core|mailing|member|contribute|...
   *
   * @return \Civi\Core\Lock\LockInterface
   *   A lock instance.
   *
   * @deprecated
   */
  public static function createCivimailLock($name) {
    $serverWideLock = \CRM_Core_BAO_Setting::getItem(
      \CRM_Core_BAO_Setting::MAILING_PREFERENCES_NAME,
      'civimail_server_wide_lock'
    );
    return new static($name, NULL, $serverWideLock);
  }

  /**
   * Initialize the constants used during lock acquire / release.
   *
   * @param string $name
   *   Symbolic name for the lock. Names generally look like
   *   "worker.mailing.EmailProcessor" ("{category}.{component}.{AdhocName}").
   *
   *   Categories: worker|data|cache|...
   *   Component: core|mailing|member|contribute|...
   * @param int $timeout
   *   The number of seconds to wait to get the lock. 1 if not set.
   * @param bool $serverWideLock
   *   Should this lock be applicable across your entire Drupal site.
   */
  public function __construct($name, $timeout = NULL, $serverWideLock = FALSE) {
    $config = CRM_Core_Config::singleton();
    $dsnArray = DB::parseDSN($config->dsn);
    $database = $dsnArray['database'];
    $domainID = CRM_Core_Config::domainID();
    if ($serverWideLock) {
      $this->name = $name;
    }
    else {
      $this->name = $database . '.' . $domainID . '.' . $name;
    }
    if (defined('CIVICRM_LOCK_DEBUG')) {
      CRM_Core_Error::debug_log_message('trying to construct lock for ' . $this->name);
    }
    $this->_timeout = $timeout !== NULL ? $timeout : self::TIMEOUT;
  }

  /**
   * Release lock on shutdown.
   */
  public function __destruct() {
    $this->release();
  }

  /**
   * Acquires the lock.
   *
   * @return bool
   *   Returns true if lock is acquired.
   */
  public function acquire($timeout = NULL) {
    $this->hasLock = lock_acquire($this->name, self::LIFETIME);
    if ($this->hasLock && defined('CIVICRM_LOCK_DEBUG')) {
      CRM_Core_Error::debug_log_message('acquire lock for ' . $this->name);
    }
    elseif (defined('CIVICRM_LOCK_DEBUG')) {
      CRM_Core_Error::debug_log_message('failed to acquire lock for ' . $this->name);
    }
    return $this->hasLock;
  }

  /**
   * Releases the lock.
   */
  public function release() {
    if ($this->hasLock) {
      try {
        lock_release($this->name);
      }
      catch (PDOException $e) {
        // If the database disconnected, we have to close and reconnect.
        db_close();
        lock_release($this->name);
      }
      if (defined('CIVICRM_LOCK_DEBUG')) {
        CRM_Core_Error::debug_log_message('release lock for ' . $this->name);
      }
      $this->hasLock = FALSE;
    }
  }

  /**
   * Returns true if lock is available.
   *
   * @return bool
   *   Returns true if lock is available.
   */
  public function isFree() {
    return lock_may_be_available($this->name);
  }

  /**
   * Returns true if lock is acquired.
   *
   * @return bool
   *   Returns true if lock is acquired.
   */
  public function isAcquired() {
    return $this->hasLock;
  }

}
